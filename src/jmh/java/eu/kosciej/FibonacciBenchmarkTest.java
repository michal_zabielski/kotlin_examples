package eu.kosciej;

import org.openjdk.jmh.annotations.*;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

@Fork(2)
@Warmup(iterations = 5)
@Measurement(iterations = 5)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class FibonacciBenchmarkTest {


    public final static long COUNT = 10000;

    @Benchmark
    public BigDecimal fibo_for_test() {
        return FibonacciKt.nth_fibo_for(COUNT);
    }

    @Benchmark
    public BigDecimal fibo_sequence_test() {
        return FibonacciKt.nth_fibo_sequence(COUNT);
    }

    @Benchmark
    public BigDecimal fibo_tailrec_test() {
        return FibonacciKt.nth_fibo_tailrec(COUNT);
    }

    @Benchmark
    public BigDecimal fibo_stream_test() {
        return FibonacciKt.nth_fibo_stream(COUNT);
    }
}
