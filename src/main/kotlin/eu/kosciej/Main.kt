package eu.kosciej

import kotlin.system.measureTimeMillis

fun main(args: Array<String>) {
    println("Hello")
//    print(tail(1))
//    fibo_stream().limit(10).forEach{ f -> println(f)}

    val el_1 = fibo_sequence().drop(5).first()
    val el_2 = nth_fibo_tailrec(5)
    val el_3 = nth_fibo_for(5)
    println("$el_1 == $el_2 ${el_1 === el_2}")
    println(el_1)
    println(el_2)
    println(el_3)
    println(el_1 == el_2)
    println(el_1 === el_2)
    for (i in 1..100) {
        val time_seq = measureTimeMillis {
            val x = fibo_sequence().drop(100000).first()
        }

        val time_tailrec = measureTimeMillis {
            val x = nth_fibo_tailrec(100000)
        }

        val time_for = measureTimeMillis {
            val x = nth_fibo_for(100000)
        }

        println("seq: $time_seq, tailrec: $time_tailrec, for: $time_for")
    }
}
