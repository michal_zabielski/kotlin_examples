package eu.kosciej

import io.reactivex.Observable
import java.math.BigDecimal
import java.util.stream.Collectors
import java.util.stream.Stream

fun fibo_rxjava(count: Int) {
    Observable.range(0, count)
            .scan(Pair(1,1), { p, _ ->  Pair(p.second, p.first+p.second)})
            .map { p -> p.second }
            .toList().blockingGet();
}

fun fibo_stream(): Stream<BigDecimal> {
    return Stream
            .iterate(Pair<BigDecimal, BigDecimal>(BigDecimal.ONE, BigDecimal.ONE), { p -> Pair(p.second, p.first + p.second) })
            .map { p -> p.first }
}

fun fibo_sequence(): Sequence<BigDecimal> {
    return generateSequence(Pair(BigDecimal.ONE, BigDecimal.ONE), { p -> Pair(p.second, p.first + p.second)})
            .map { p -> p.first }
}

// returns sequence of Pairs with fibonacci number and sequential number of fibonacci number
fun fibo_sequence_with_numbers(): Sequence<Pair<Long, BigDecimal>> {
    return generateSequence(0, Long::inc) zip fibo_sequence()
}

// nth_* functions get nth Fibonacci element

fun nth_fibo_sequence(element: Long) : BigDecimal {
    return fibo_sequence().drop(element.toInt()).first()
}

fun nth_fibo_stream(element: Long): BigDecimal {
    return fibo_stream().skip(element).limit(1).collect(Collectors.toList()).get(0)
}

fun nth_fibo_for(count: Long): BigDecimal {
    var first: BigDecimal = BigDecimal.ONE
    var second: BigDecimal = BigDecimal.ONE
    for (i in 1..count) {
        val temp = first
        first = second
        second = temp + second
    }
    return first
}

fun nth_fibo_tailrec(count: Long): BigDecimal {
    tailrec fun f(nth: Long, first: BigDecimal, second: BigDecimal): BigDecimal {
        return if (nth == 0.toLong())
            first
        else f(nth -1, second, first + second)
    }

    return f(count, BigDecimal.ONE, BigDecimal.ONE)
}